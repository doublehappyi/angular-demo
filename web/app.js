/**
 * Created by db on 16/1/19.
 */
var opr = angular.module('opr', ['ngRoute', 'UserControllers']);

opr.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/user/login', {
        templateUrl: 'user/partials/login.html',
        controller: 'LoginCtrl'
    }).when('/role', {
        templateUrl: 'role/partials/index.html',
        controller: 'ToolbarCtrl'
    }).otherwise('/user/login', {
        templateUrl: 'user/partials/login.html',
        controller: 'LoginCtrl'
    });
}]);